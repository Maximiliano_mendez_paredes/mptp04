import os

#Menu
def menu():
    os.system ('cls')
    print ('1) registrar productos')
    print ('2) mostrar el listado de productos')
    print ('3) mostrar productgos dentro de un intervalo')
    print ('4) agregar cantidad de productos a los productos con un stock menor al que se indique')
    print ('5) eliminar productos cuyo stock sea 0 ')
    print ('6) salir')

    opc = int (input ('ingrese una opcion '))
    while not (opc >= 1 and opc <= 6):
        opc = int (input('ingrese una opcion '))
    return opc

def registrar_productos():
    productos = {}

    resp = 's'
    while resp == 's':
        codigo = int (input('ingrese codigo del producto '))
        if codigo not in productos:
            descripcion = input ('ingrese descrpcion del producto ')
            precio = ingresar_precio()
            stock = ingresar_stock()
            productos [codigo] = [descripcion,precio,stock]
            print ('se cargo correctamente ')
        else:
            print ('codigo ya existe ')
        resp = input ('desea continuar cargando ? s/n ')
    print ('se finalizo carga de productos')    
    return productos               
def ingresar_precio():
    precio = float (input ('ingrese el precio del producto '))
    while not precio >= 0:
        precio = float (input ('ingrese el precio del producto '))
    return precio    

def ingresar_stock():
    stock = int (input('ingrese stock del producto '))
    while not stock >= 0:
        stock = int (input('ingrese stock del producto '))
    return stock    

def mostrar_productos():
    print('listado de productos ')
    for clave, valor in productos.items():
        print (clave,valor)
    
def intervalo_productos(productos):
    dic2 = {}
    a = int(input('ingrese valor inicial de stock a buscar '))
    b = int(input('ingrese valor final de stock a buscar '))
    for i in productos:
        c = productos [i][2]
        if (c >= a and c <= b):
            d = i
            e = productos.get(i)
            dic2 [d] = e
    return dic2        

def agregar_cantidad(productos):
    a = int(input('ingrese cantidad a incrementar '))
    b = int(input('ingrese valor de stock para incrementar '))
    for i in productos:
        c = productos [i][2]
        if c <= b:
            c += a
            productos [i][2] = c

    return 'modificado correctamente' 

def eliminar_productos (productos):
    resultado = {}
    resp = input('usted desea todos los elementos cuyo stock es igual a cero ? s/n ')
    if resp == 's':
        for i in productos:
            a = productos [i][2]
            if a == 0:
                del productos[i]    
        print ('se elimino todo correctamente ')

    else:
        print ('eliminacion cancelada ')  
                  



#principal
os.system ('cls')

opc = 6
while (opc >= 1 and opc <= 6):

    opc = menu ()

    if opc == 1:
        productos = registrar_productos()
        input ('presione un boton para continuar ')
    elif opc == 2:
        print (mostrar_productos())
        input ('presione un boton para continuar')
    elif opc == 3:
        print (intervalo_productos(productos))
        input ('presione un boton para continuar')
    elif opc == 4:
        print (agregar_cantidad(productos))
        input ('presione un boton para continuar')
    elif opc == 5:
        print (eliminar_productos(productos))
        input ('presione un boton para continuar ')
    elif opc == 6:
        print ('fin del programa...')
        input ('presione un boton para finalizar ')                    


